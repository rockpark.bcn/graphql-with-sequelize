import Sequelize, {Model} from 'sequelize';

class Brand extends Model {
    static tableName = 'brands';

    static associate(models) {
        Brand.Car = Brand.hasMany(models.Car, {
            foreignKey: 'brand_id',
            as: 'cars',
        });
    }
}

const schema = {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    name: Sequelize.STRING,
    intro: Sequelize.STRING,
    type: {
        type: Sequelize.ENUM('interior', 'exterior'),
    },
};

export default (sequelize) => {
    Brand.init(schema, {
        sequelize,
        tableName: Brand.tableName,
        timestamps: true,
        underscored: true,
        paranoid: true,
    });

    return Brand;
};
