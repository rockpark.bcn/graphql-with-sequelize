import jwt from 'jsonwebtoken';
import _ from 'lodash';
import bcrypt from 'bcrypt';

export const CreateTokens = async(user, secretKey, secretKey2) =>{
    const createToken = jwt.sign(
        {
            user: _.pick(user, ['id', 'email', 'name'])
        },
        secretKey,
        {
            expiresIn: '1h'
        },
    );

    const createRefreshToken = jwt.sign(
        {
            user: _.pick(user, ['id', 'email', 'name'])
        },
        secretKey2,
        {
            expiresIn: '1h'
        },
    );

    return [createToken, createRefreshToken];

};