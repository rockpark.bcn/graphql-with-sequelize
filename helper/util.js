'use strict';

import m from "moment-timezone";

export const getNow = () => {
  return m.tz(new Date(), 'Asia/Seoul').format("YYYY-MM-DD HH:mm:ss");
};
