'use strict';

import faker from 'faker';
import {getNow} from '../helper/util';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const users = [];
    for (let i = 1; i <= 100; i++) {
      users.push({
        id: faker.random.word(),
        email: faker.internet.email(),
        password: "123123",
        name: faker.fake("{{name.lastName}}, {{name.firstName}}"),
        type: "general",
        created_at: getNow(),
        updated_at: getNow(),
      });
    }

    return queryInterface.bulkInsert('users', users, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
