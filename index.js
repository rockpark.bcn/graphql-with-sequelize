import { GraphQLServer } from "graphql-yoga";
import resolvers from './graphql/resolvers';
import { createContext, EXPECTED_OPTIONS_KEY } from 'dataloader-sequelize';
import models from './models';

// resolver.contextToOptions = { [EXPECTED_OPTIONS_KEY]: EXPECTED_OPTIONS_KEY };

const server = new GraphQLServer({
    typeDefs: "graphql/schema.graphql",
    resolvers,
    context(req) {
        // For each request, create a DataLoader context for Sequelize to use
        const dataloaderContext = createContext(models.sequelize);

        // Using the same EXPECTED_OPTIONS_KEY, store the DataLoader context
        // in the global request context
        return {
            [EXPECTED_OPTIONS_KEY]: dataloaderContext,
        };
    },
});

const options = {
    port: 4000,
    endpoint: '/api',
    subscriptions: '/subscriptions',
    playground: '/play',
};

server.start(
    options,
    () => console.log("GraphQL Server Running")
);
